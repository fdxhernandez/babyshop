<?php 
include_once ('./controlador/c_index.php');
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Luis fernando hernandez, desarrollador web , plantilla free bootstrap">
    <meta name="description" content="Babyshop, venta de ropa infantil por internet en colombia, envios a todo el pais">
    <meta name="keywords" content="ropa,bebes,bebe, niños, niñas, online">
    <link href="./img/previa.png" rel="image_src"/>


    <title>BabyShop Kids - Moda Infantil</title>
    <link href="./img/previa.png" rel="image_src"/>
    <link rel="icon" type="image/png" href="./ico/bsk.png">
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/creative.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
</head>

<body id="page-top">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <div style="float:left !important; margin-left: 10px;"><a class="navbar-brand page-scroll " href="#page-top">BabyShop Kids</a></div>
            </div>
             <div>
                <div class="grises"><a href="#page-top"><img src="img/logo.png" class="imagen">
                </a></div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">Quienes Somos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Productos</a>                        
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Servicio A todo el pais</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contacto</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>

        <div class="header-content">
            <div class="header-content-inner">

                <h1 id="homeHeading" class="tituobsk">BabyShop Kids</h1>
                <hr>
                <br>
                <a href="#about" class="btn btn-primary btn-xl page-scroll center">Conoce más de nosotros!!!</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">¿Quienes somos?</h2>
                    <hr class="light">
                    <p class="text-faded">Somos una tienda virtual, dedicada a la comercialización de ropa importada de linea infantil</p>
                    
                    <a href="#services" class="page-scroll btn btn-default btn-xl sr-button" id="servicios">Sevicios!</a>
                </div>
            </div>
           
        </div>
    </section>

    

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
            <?php foreach ($prod as $productos){ ?>
                <div class="col-lg-4 col-sm-6">
                    <a href="<?php 
                            $link=$productos['prod_urlimg'];
                        $ruta = substr($link, 1);
                        echo $ruta; ?>"" class="portfolio-box">
                        <img src="<?php 
                        echo $ruta; ?>" class="img-responsive" alt="<?php echo $productos['prod_nombre'];?>">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    <?php echo $productos['prod_nombre'];?>
                                </div>
                                <div class="project-name">
                                    <?php echo $productos['prod_descripcion'];?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
<section id="services">
        <div class="container">
             
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Baby shop</h2>
                    <hr class="primary">
                    
            <?php include("carrusel.html") ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
                        <h3>Sturdy Templates</h3>
                        <p class="text-muted">Our templates are updated regularly so they don't break.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
                        <h3>Envios seguros</h3>
                        <p class="text-muted">Envios seguros a nivel navional!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-shopping-bag text-primary sr-icons"></i>
                        <h3>Up to Date</h3>
                        <p class="text-muted">We update dependencies to keep things fresh.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-heart text-primary sr-icons"></i>
                        <h3>Made with Love</h3>
                        <p class="text-muted">You have to make your websites with love these days!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <aside class="bg-bsk">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Nuestras Redes Sociales!</h2>
                 </div class="container">
            <div class="col-lg-12 text-center col-md-12 col-xs-12"> 
                 <?php foreach ($cont as $contactoinfo){ ?>
                <a href="<?php echo $contactoinfo['perfil_facebook'];?>" target="_blank"><i class="col-lg-3 col-md-3 col-xs-3 fa fa-facebook-square fa-2x " ></i></a>
                <a href="<?php echo $contactoinfo['instagram'];?>" target="_blank"><i class="col-lg-3 col-md-3 col-xs-3 fa fa-instagram fa-2x"></i></a>
                <a href="<?php echo $contactoinfo['fanpage_facebook'];?>" target="_blank"><i class="col-lg-3 col-md-3 col-xs-3 fa fa-facebook fa-2x"></i></a>
                <a href="<?php echo $contactoinfo['twitter'];?>" target="_blank"><i class="col-lg-3 col-md-3 col-xs-3 fa fa-twitter fa-2x"></i></a>
            </div>
        </div>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Contactanos!!!</h2>
                    <hr class="primary">
                    <p>Envios a todo el pais, a el mejor precio</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p><?php echo $contactoinfo['celular'];?></p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:<?php echo $contactoinfo['correo'];?>"><?php echo $contactoinfo['correo'];?></a></p>
                </div>
            </div>
        </div>
    </section>
            <?php }?>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>

</body>

</html>
