<?php 
include_once ('../controlador/c_usuarios.php');  
include('../controlador/sec.php'); 
$roluser = isset($_SESSION["rol"]) ? $_SESSION["rol"]:NULL;
$id = isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"]:NULL;
if ($roluser != "superadmin"){
    header("Location:../vista/editUsuario.php?id=".$id);
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración Baby Shop Kids - Moda Infantil</title>
        <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
</head>
<body>
<div id="contenedor col-md-12 ">
    <header>
        <br>
        <div id="user">
            <?php include("v_user.php"); ?>
        </div>
    </header>
    <div>
        <?php include('menu.php'); ?>
    </div>
    <div class="trabajo col-md-6 ">
            <div class="form-group col-md-12">
                <h1>Crear Usuario</h1>
                <p>*Campos Obligatorios</p>
                <form method="post" action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Nombre Usuario">*Nombre:</label>
                        <div class="col-sm-8">
                            <input type="text" name="nombre" placeholder="Nombre de Usuario" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Correo Usuario">*Correo:</label>
                        <div class="col-sm-8">
                            <input type="email" name="correo" placeholder="Dirección de Correo" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Contraseña Usuario">*Contraseña:</label>
                        <div class="col-sm-8">
                            <input type="text" name="password" placeholder="Contraseña" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Rol Usuario">*Rol:</label>
                        <div class="col-sm-8">
                            <select name="rol" class="form-control">
                                <?php foreach($filaroles as $rol) {?>
                                <option value="<?php echo $rol['rol_id']; ?>"><?php echo $rol['rol_nombre']; ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" value="Registrar" class="btn btn-success">
                        </div>
                    </div>
                </form>
            <h1>Usuarios</h1>
            <div class="table-responsive">
                <table class="table table-bordered table-hover"> 
                    <tr class="active">
                        <td align="center">habilitar <br>o inhabilitar</td>
                        <td>Nombre</td>
                        <td>correo</td>
                        <td>rol</td>
                        <td>estado</td>
                        <td>editar</td>
                    </tr>
                    <?php foreach ($filas as $fila){ ?>
                    <tr>
                        <?php if($fila["esta_nombre"]==="activo"){?>
                        <td align="center"><a href='../vista/crearUsuario.php?id_del=<?php echo $fila["usu_id"]; ?>'><img src='../ico/habilitado.png'> </a></td>
                        <?php }else{ ?>
                        <td align="center"><a href='../vista/crearUsuario.php?id_act=<?php echo $fila["usu_id"]; ?>'><img src='../ico/inhabilidato.png'></a></td>
                        <?php }?>
                        <td><?php echo $fila["usu_nombre"];?></td>
                        <td><?php echo $fila["usu_correo"];?></td>
                        <td><?php echo $fila["rol_nombre"];?></td>
                        <td><?php echo $fila["esta_nombre"];?></td>
                        <td><a href='../vista/editUsuario.php?id=<?php echo $fila["usu_id"]; ?> '><img src='../ico/editar.png'></a></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
    </div>
    </div>
    <footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>   
    </footer>
</body>
</html>