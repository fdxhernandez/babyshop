<?php
include_once ('../controlador/c_editProducto.php');
include('../controlador/sec.php'); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración Baby Shop Kids - Moda Infantil</title>
        <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script>
        <script language="JavaScript" type="text/javascript" src="../js/producto.js"></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link href="//cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <!--script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script-->
        <script src="../js/cdn-datatables.js"></script>
        <script src="///cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
</head>
<body>
<div id="contenedor col-md-12 ">
    <header>
        <br>
        <div id="user">
            <?php include("v_user.php"); ?>
        </div>
    </header>
    <div>
        <?php include('menu.php'); ?>
    </div>
    <div class="trabajo col-md-6 ">
        <?php
        if($listapro){
        foreach ($listapro as $fila){?>
            <div class="form-group">
                <h1>Editar Producto (<?php echo $fila['prod_nombre']; ?>)</h1>
                <p>*Campos obligatorios</p>
                <form method="POST" action="" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Nombre producto a editar">*Nombre:</label>
                        <div class="col-sm-8">
                            <input type="text" name="nombre" id="nombrepro" value="<?php echo $fila['prod_nombre']; ?>" required class="form-control" placeholder="Nombre Producto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Imagen producto a editar">Imagen:</label>
                        <div class="col-sm-8">
                            <input type="file" name="imagen" id="imagenpro" class="form-control">
                            <input type="hidden" name="img" value="<?php echo $fila['prod_urlimg']; ?>">
                            <input type="hidden" name="id" value="<?php echo $fila['prod_id']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="descripcion producto a editar">*descripcion:</label>
                        <div class="col-sm-5 col-md-4">
                            <textarea rows="10" cols="50" class="form-control" name="descripcion" id="descripcionpro" placeholder="Descripcion..." maxlength="500" required><?php echo $fila['prod_descripcion']; ?></textarea>
                        </div>
                        <div class="col-sm-4 col-md-5 ">
                            <img id="img_destino" src="<?php echo $fila['prod_urlimg']; ?>" alt="Vista Previa" class=" img-thumbnail img-responsive">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="precio producto a editar">precio:</label>
                        <div class="col-sm-8">
                            <input type="number" name="precio" id="preciopro" value="<?php echo $fila['precio']; ?>" class="form-control" placeholder="Precio">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="edad producto a editar">*Edad:</label>
                        <div class="col-sm-2">
                            <select name="edad" id="edadpro" class="form-control">
                            <option value="<?php echo $fila['id_edad']; ?>"> <?php echo $fila['edad_nombre']; ?></option>
                                <?php foreach($filaedad as $edades) {?>
                                <option value="<?php echo $edades['edad_id']; ?>" ><?php echo $edades['edad_nombre']; ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                        <label class="control-label col-sm-1" for="Genero producto a editar">*Genero:</label>
                        <div class="col-sm-2">
                            <select name="Genero" id="Generopro" class="form-control">
                            <option value="<?php echo $fila['id_genero']; ?>"> <?php echo $fila['gen_nombre']; ?></option>
                                <?php foreach($filagenero as $generos) {?>
                                <option value="<?php echo $generos['gen_id']; ?>"><?php echo $generos['gen_nombre']; ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                        <label class="control-label col-sm-1" for="estado producto a editar">*Estado:</label>
                        <div class="col-sm-2">
                            <select name="estado" id="estadopro" class="form-control">
                             <option value="<?php echo $fila['id_estpro']; ?>"> <?php echo $fila['estpro_nombre']; ?></option>
                                <?php foreach($filaestpro as $estpro) {?>
                                <option value="<?php echo $estpro['estpro_id']; ?>"><?php echo $estpro['estpro_nombre']; ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" value="Actualizar" name="Actualizar" 
                             class=" btn btn-success">
                             <input type="button" onclick=" location.href='../vista/listaproducto.php' " value="Volver" name="boton" class="btn btn-info" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>          
        

      
        
  <?php      }
}

?>


<footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>   
</footer>
</body>
</html>


       
  
         
