<?php include_once ('../controlador/c_login.php'); ?>
 <!DOCTYPE html>
<html lang="es">
<head>
    <title>Login Administración Baby Shop Kids - Moda Infantil</title>
    <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
        <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
</head>
<body>


<?php
//include('vista/listaproducto.php'); ?>
<div id="contenedor">
    <div class="Login">
        <div class="row">
            <img src="../img/logo.png" class="loginimg">
            <H3>Sistema de Administración Babyshop Kids web</H3>
            <br><br>
            <div>
               <form name="login" action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-offset-5 col-md-3">
                        <div class="form-login">
                            <h4>Login</h4> 
                            <input name="usuario" required type="text" placeholder="Correo" class="form-control input-sm chat-input"/>    
                            <br>                 
                            <input name="password" required type="password" placeholder="Contraseña" class="form-control input-sm chat-input" /> 
                            <br>
                            <div class="wrapper">
                                <span class="group-btn">                      
                                    <input type="submit" value="Ingresar" name="login" class="btn btn-primary btn-md"/> <i class="fa fa-sign-in"></i>
                                </span>
                            </div>
                        </div>
                    </div>                                
                </form>
            </div>
        </div>
    </div>
</div>
<footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>
        
</footer>
</body>
</html>