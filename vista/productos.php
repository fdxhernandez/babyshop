<?php 
    include_once ('../controlador/c_productos.php'); 
    include('../controlador/sec.php'); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración Baby Shop Kids - Moda Infantil</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <!-- <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script> //-->
        <script src = "//code.jquery.com/jquery-1.x-git.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="../js/producto.js"></script>
        <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
</head>
<body>
    <div id="contenedor">
        <header>
            <br>
            <div id="user">

                <?php include("v_user.php"); ?>
            </div>
        </header>
        <div>
            <?php include('menu.php'); ?>
        </div>
        <div class="trabajo col-lg-12 ">
            <table class="table-condensed ">
            <tr>
                <td colspan="2"><h3>Crear Edad</h3></td>
                <td colspan="2"><h3>Crear Genero</h3></td>
                <td colspan="2"><h3>Crear Estado</h3></td>
            </tr>
            <tr>
            <form name="edades" action="" method="post" >
                <td><input type="text" name="crearedad" id="Edad" placeholder="Edad..." class="form-control"></td>
                <td><input type="button" name="Guardaredad" value="Guardar" id="Guardaredad" class="btn btn-success"></td>
            </form>
            <form name="generos" action="" method="post" >
                <td><input type="text" name="creargenero" id="genero" placeholder="Genero..." class="form-control"></td>
                <td><input type="button" name="Guardargenero" value="Guardar" id="Guardargenero" class="btn btn-success"></td>
            </form>
            <form name="estado" action="" method="post" >
                <td><input type="text" name="crearestado" id="newestado" placeholder="Estado..." class="form-control"></td>
                <td><input type="button" name="Guardarestado" value="Guardar" id="Guardarestado" class="btn btn-success"></td>
            </form>
            </tr>
            </table>
        <div id="mensaje"></div>
        <div id="productofinal" class="col-lg-12"><?php include('productofinal.php');?></div>
        </div>
    </div>
    <footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>   
    </footer>
</body>
</html>