<?php
include_once ('../controlador/c_productos.php');
if(isset($_GET["ajax"]))
{
    include('../controlador/sec.php'); 
}
//echo "Valor: $autent"; exit;
if ($autent != "yes") 
{
    //si no existe, envio a la p�gina de autentificacion
    session_destroy();
    header("Location:../vista/productos.php");
    exit();
}
?>

    <div class="form-group">
        <form method="post" action="" enctype="multipart/form-data"  id="form1" name="form1" class="form-horizontal">
            <h1>Crear Producto</h1>
            <p>*Campos Obligatorios</p>
            <div class="form-group">
                <label class="control-label col-sm-2" for="Nombre Producto">*Nombre: </label>
                <div class="col-sm-8">
                    <input type="text" name="nombre" id="nombrepro" class="form-control" required placeholder="Nombre Producto">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="Imagen Producto">*Imagen: <br> (tamaño de 650ancho x 350alto)</label>
                <div class="col-sm-8">
                    <input type="file" name="imagen" id="imagenpro" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="descripcion Producto">*Descripcion: </label>
                <div class="col-sm-5 col-md-4">
                    <textarea rows="10" cols="50" class="form-control" name="descripcion" id="descripcionpro" placeholder="Descripcion..." maxlength="500" required></textarea>
                </div>
                <div class="col-sm-4 col-md-4 ">
                <img id="img_destino" src="../img/prov.png" alt="Vista Previa" class=" img-thumbnail img-responsive ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="precio Producto">precio: </label>
                <div class="col-sm-8">
                    <input type="number" name="precio" id="preciopro" placeholder="precio Producto" class="form-control" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="edad Producto">*Edad: </label>
                <div class="col-sm-8">
                    <select name="edad" id="edadpro" class="form-control">
                        <?php foreach($filaedad as $edades) {?>
                        <option value="<?php echo $edades['edad_id']; ?>" ><?php echo $edades['edad_nombre']; ?></option>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="Genero Producto">*Genero: </label>
                <div class="col-sm-8">
                    <select name="Genero" id="Generopro" class="form-control">
                       <?php foreach($filagenero as $generos) {?>
                        <option value="<?php echo $generos['gen_id']; ?>"><?php echo $generos['gen_nombre']; ?></option>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="estado Producto">*Estado: </label>
                <div class="col-sm-8">
                    <select name="estado" id="estadopro" class="form-control">
                        <?php foreach($filaestpro as $estpro) {?>
                        <option value="<?php echo $estpro['estpro_id']; ?>"><?php echo $estpro['estpro_nombre']; ?></option>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" value="Registrar" name="btnguard" class="btn btn-success">
                    <input type="hidden" name="envg" value="true">
                    <input type="button" onclick=" location.href='../vista/listaproducto.php' " value="Ver Productos" name="boton" class="btn btn-info"/>
                </div>
            </div>
        </form>
    </div>
    
