<?php 

$roluser = isset($_SESSION["rol"]) ? $_SESSION["rol"]:NULL;
$id = isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"]:NULL;

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
<title>Untitled Document</title>
</head>

<body>
<ul id="menu">
<li>
	<img src="../img/logo.png" class="img-responsive img-home col-sm-4"  alt="Logo Babyshop Kids">
</li>
	<li><a href="../vista/contacto.php">contacto</a>
	</li>
    
	<li>
		<a href="../vista/productos.php">Productos</a>
		<ul>
			<li><a href="../vista/productos.php">Ingresar Producto</a></li>
			<li><a href="../vista/listaproducto.php">Listado Productos</a></li>
		</ul>
	</li>
	</li>
    
	<li><a <?php if ($roluser != "superadmin")  {?> href="../vista/editUsuario.php?id=<?php echo $id;?>"<?php }else{?> href="../vista/crearUsuario.php"<?php }?> > Usuarios</a>

	</li>
      </li>
</ul>
</body>
