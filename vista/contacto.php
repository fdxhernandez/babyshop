<?php 
include_once ('../controlador/c_contacto.php'); 
include('../controlador/sec.php'); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración Baby Shop Kids - Moda Infantil</title>
    <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script>
        <script language="JavaScript" type="text/javascript" src="../js/producto.js"></script>
        <link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">


</head>
<body>

<div id="contenedor col-md-12 ">
    <header>
        <br>
        <div id="user">
            <?php include("v_user.php"); ?>
        </div>
    </header>
    <div>
        <?php include('menu.php'); ?>
    </div>
    <div class="trabajo col-md-6 ">     
        <div class="form-group">
            <h1>Información de contacto</h1>
            <p>*Campos Obligatorios</p>
            <?php if($info)
                foreach ($info as $infocontact){ ?>
                <form method="post" action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Facebook FanPage">*Fan page Facebook:</label>
                        <div class="col-sm-8">
                            <input type="text" name="fanpage_facebook" value="<?php echo $infocontact['fanpage_facebook']; ?>" class="form-control" required placeholder="Link FanPage En Facebook">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="instagram">*Instagram:</label>
                        <div class="col-sm-8">
                            <input type="text" name="instagram" value="<?php echo $infocontact['instagram']; ?>" class="form-control"  required placeholder="Link Perfil En Instagram">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Perfil Facebook">*Perfil Facebook:</label>
                        <div class="col-sm-8">
                            <input type="text" name="perfil_facebook" value="<?php echo $infocontact['perfil_facebook']; ?>" class="form-control"  required placeholder="Link Perfil En Facebook">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="twitter">Twitter:</label>
                        <div class="col-sm-8">
                            <input type="text" name="twitter" value="<?php echo $infocontact['twitter']; ?>" class="form-control" placeholder="Link Perfil En Twitter">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Celular">Celular:</label>
                        <div class="col-sm-8">
                            <input type="text" name="celular" value="<?php echo $infocontact['celular']; ?>" class="form-control" placeholder="celular">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Correo">Correo:</label>
                        <div class="col-sm-8">
                            <input type="email" name="correo" value="<?php echo $infocontact['correo']; ?>" class="form-control" placeholder="Direccion de Correo Electronico" >
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" value="Guardar" name="actuinfo" class="btn btn-success">
                        </div>
                    </div>
                </form>
            <?php }?>
        </div>   
    </div>
</div>

<footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>   
</footer>
</body>
</html>