<?php 
include_once ('../controlador/c_productos.php');  
include('../controlador/sec.php'); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración Baby Shop Kids - Moda Infantil</title>
        <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script>
        <script language="JavaScript" type="text/javascript" src="../js/producto.js"></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link href="//cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">
		<!--script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script-->
        <script src="../js/cdn-datatables.js"></script>
		<script src="///cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
</head>
<body>
<div id="contenedor col-md-12 ">
    <header>
        <br>
        <div id="user">
            <?php include("v_user.php"); ?>
        </div>
    </header>
    <div>
        <?php include('menu.php'); ?>
    </div>
    <div class="trabajo col-md-6 ">   
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Productos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Listado de productos
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ListaProductos">
                                <thead>
                                    <tr>
                                        <th>eliminar</th>
						                <th>Nombre</th>
						                <th>Descripcion</th>
						                <th>Imagen</th>
						                <th>Precio</th>
						                <th>Edad</th>
						                <th>Genero</th>
						                <th>Estado de producto</th>
						                <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php foreach ($listaproducto as $lproducto){ ?>
                                    <tr class="odd gradeX">
                                        <td><input type="button" name="buton" class="deluser" id="<?php echo $lproducto["prod_id"];?>" value="<?php echo $lproducto["prod_id"];?>"></td>
						                <td><?php echo $lproducto["prod_nombre"];?></td>
						                <td><?php echo $lproducto["prod_descripcion"];?></td>
						                <td><img src="<?php echo $lproducto["prod_urlimg"];?>" widht="50" height="50"></td>
						                <td><?php echo $lproducto["precio"];?></td>
						                <td><?php echo $lproducto["edad_nombre"];?></td>
						                <td><?php echo $lproducto["gen_nombre"];?></td>
						                <td><?php echo $lproducto["estpro_nombre"];?></td>
						                <td><a href='../vista/editProducto.php?idp=<?php echo $lproducto["prod_id"]; ?> '><img src='../ico/editar.png'></a></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                </div>
</div>

<footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>   
</footer>
</body>
</html>