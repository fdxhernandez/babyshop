<?php
include_once ('../controlador/c_editUsuario.php');
include('../controlador/sec.php'); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración Baby Shop Kids - Moda Infantil</title>
        <link rel="icon" type="image/png" href="../ico/bsk.png">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="//code.jquery.com/jquery-2.2.4.min.js" ></script>
        <script language="JavaScript" type="text/javascript" src="../js/producto.js"></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link href="//cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <!--script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script-->
        <script src="../js/cdn-datatables.js"></script>
        <script src="///cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/stylemen.css" type="text/css" />
        <link rel="stylesheet" href="../css/home.css" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
</head>
<body>
<div id="contenedor col-md-12 ">
    <header>
        <br>
        <div id="user">
            <?php include("v_user.php"); ?>
        </div>
    </header>
    <div>
        <?php include('menu.php'); ?>
    </div>
    <div class="trabajo col-md-6 ">   
        <div class="row">
            <div class="col-lg-12 form-group">
                <?php if($filas){
                    foreach ($filas as $fila){?>
                        <h1>Editar Usuario</h1>
                        <p>*Campos Obligatorios</p>
                        <form method="post" action="" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Nombre Usuario">*Nombre:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="nombre" value="<?php echo $fila['usu_nombre']; ?>" class="form-control" required></td>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Correo Usuario">*Correo:</label>
                                <div class="col-sm-8">
                                    <input type="email" name="correo" value="<?php echo $fila['usu_correo']; ?>" required class="form-control" ></td>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Contraseña Usuario">*Contraseña:</label>
                                <div class="col-sm-8">
                                    <input type="password" name="password" value="<?php echo $decrypted;?>" required class="form-control" ></td>
                                </div>
                            </div>
                            <?php $roles=$rol; if ($rol == "superadmin")  {?>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Rol Usuario">*Rol:</label>
                                <div class="col-sm-8">
                                <select name="rol" class="form-control">
                                    <?php foreach($filasroles as $rol) {?>
                                    <option value="<?php echo $rol['rol_id']; ?>"  ><?php echo $rol['rol_nombre']; ?></option>
                                    <?php  } ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Estado Usuario">*Estado:</label>
                                <div class="col-sm-8">
                                <select name="estado" class="form-control">
                                    <?php foreach($filaestados as $estados) {?>
                                    <option value="<?php echo $estados['esta_id']; ?>"><?php echo $estados['esta_nombre']; ?></option>
                                    <?php  } ?>
                                </select>
                                </div>
                            </div>
                            <?php }else{ ?>
                                <input type="hidden" name="rol" value="1">
                                <input type="hidden" name="estado" value="1">

                            <?php
                               } ?>
                            <div class="form-group"> 
                                <div class="col-sm-offset-2 col-sm-10">
                                    <input type="submit" name="guardar" value="Guardar" class="btn btn-success"> 
                                    <input type="hidden" value="<?php echo $fila['usu_id']; ?>" name="id">
                                    <?php if ($roles == "superadmin")  {?> 
                                    <input type="button" onclick="location.href='crearUsuario.php';" value="Volver" class="btn btn-primary" />
                                    <?php }?>
                                </div>
                            </div>
                        </form>
            <?php   }
                }?>
            </div>
        </div>
    </div>
<footer> 
        <p><span > Desarrollado por:  Luis Fernando Hernandez --Tencologo en Analisis y desarrollo de software y sistemas de información-- Colombia, 2016 </span></p>   
</footer>
</body>
</html>