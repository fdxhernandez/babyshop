<?php
require_once ('../modelo/class.conexion.php');
require_once ('../modelo/class.producto.php');
require_once ('../modelo/class.edad.php');
require_once ('../modelo/class.estadoproducto.php');
require_once ('../modelo/class.genero.php');

$mensaje=NULL;
//listar edades
$edad=new edad();
$filaedad=$edad->ListarEdades();
//listarestadoproducto
$estadoproducto=new estadoProducto();
$filaestpro=$estadoproducto->Listarestadoproducto();
//listar generos
$genero=new genero();
$filagenero=$genero->ListarGenero();
 

if(isset($_GET['idp'])){
  $producto= new producto();
  $idP=$_GET['idp'];
  $listapro=$producto->CargaProdEDICT($idP);
  if(isset($_POST['Actualizar'])){
    $id=$_POST['id'];
    $prod_nombre=$_POST['nombre'];
    $prod_descripcion=$_POST['descripcion'];
    $tmpimagen = $_FILES['imagen']['tmp_name'];
    $img=$_POST['img'];
    $precio=$_POST['precio'];
    $id_edad=$_POST['edad'];
    $id_genero=$_POST['Genero'];
    $id_estpro=$_POST['estado'];
    if(strlen($id)>0 &&  strlen($prod_nombre)>0 && strlen($prod_descripcion)>0 && strlen($img)>0 && strlen($id_edad)>0 && strlen($id_genero)>0 && strlen($id_estpro)>0 ){
      if(strlen($tmpimagen)>0){
        $imaf=getimagesize($tmpimagen);
        $alto=$imaf[1];
        $ancho=$imaf[0];
        $peso_imagen = $_FILES['imagen']['size'];
        $tipo_imagen = $_FILES['imagen']['type'];
        $extensionnew = substr($tipo_imagen, -3);
        $extensionant = substr($img, -3); 
          if($extensionnew!=$extensionant){
            $type = explode("/", $tipo_imagen); 
            $extension = end($type);
            $type = explode(".", $img); 
            $extensionant = end($type);
            $nameimagen=str_replace($extensionant, $extension, $img);
            unlink($img);
          }else{
            $ruta = substr($img, 0,28);
            $nombre = substr($img, 28);
            $nombremodif = substr($img, 28,-5);
            $ex=substr($img, -5);
            $nameimagen=$ruta.$nombremodif."1".$ex;
            unlink($img);
          }
          if($alto<=350 && $alto>=340 && $ancho<=650 && $ancho>=640 ){
            if($peso_imagen<= 2097152){
              if($tipo_imagen == "image/jpeg" || $tipo_imagen == "image/png" || $tipo_imagen == "image/jpeg"){
                copy($tmpimagen,$nameimagen);
                $mensaje=$producto->actualizarProducto($id,$prod_nombre,$prod_descripcion,$nameimagen,$precio,$id_edad,$id_genero,$id_estpro);
                  echo "<script languaje='javascript'>alert('$mensaje')</script>";
                  echo "<script type='text/javascript'>window.location='../vista/listaproducto.php';</script>"; 
              }else{
                echo "<script languaje='javascript'>alert('Formato de imagen invalido, debe ser jpg')</script>";
              }
            }else{
              echo "<script languaje='javascript'>alert('imagen no debe superar 2 megas de peso')</script>";
            }
          }else{
            echo "<script languaje='javascript'>alert('Medidas de la imagen deben de ser de 650 de ancho por 350 de alto')</script>";
          }
      }else{
        $mensaje=$producto->actualizarProducto($id,$prod_nombre,$prod_descripcion,$img,$precio,$id_edad,$id_genero,$id_estpro);
        echo "<script languaje='javascript'>alert('$mensaje')</script>";
        echo "<script type='text/javascript'>window.location='../vista/listaproducto.php';</script>"; 
      }
    }else{
      echo "<script languaje='javascript'>alert('Por favor completar todos los campos')</script>";
    }
  }
}else{
echo "<script type='text/javascript'>window.location='../vista/listaproducto.php';</script>";
}

?>