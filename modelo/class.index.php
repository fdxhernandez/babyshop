<?php
class index{

	public function Productos() {
        $rows=NULL;
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql = "SELECT producto.prod_id, producto.prod_nombre, producto.prod_descripcion, producto.prod_urlimg, producto.precio, edad.edad_nombre, genero.gen_nombre, estado_producto.estpro_nombre FROM producto INNER JOIN edad ON producto.id_edad=edad.edad_id INNER JOIN genero ON producto.id_genero=genero.gen_id INNER JOIN estado_producto on producto.id_estpro=estado_producto.estpro_id WHERE producto.id_estpro=1 ORDER BY prod_id";
        $statement = $conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function InfoContacto(){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        $sql="SELECT * FROM contacto WHERE id_info=1 ";
        $statement=$conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

}
?>