<?php
class contacto{
    
    public function ListarInfoContacto(){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        $sql="SELECT * FROM contacto WHERE id_info=1 ";
        $statement=$conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    
   
    
     //ActualizarInformacion
    public function ActualizarInfoContacto($fanpage_facebook,$instagram,$perfil_facebook,$twitter,$celular,$correo){
        $modelo = new Conexion();
        $conexion = $modelo->get_conexion();
       $sql="UPDATE contacto SET fanpage_facebook=:fanpage_facebook,instagram=:instagram,perfil_facebook=:perfil_facebook,twitter=:twitter,celular=:celular,correo=:correo WHERE id_info=1";
       $statement=$conexion->prepare($sql);
        $statement->bindParam(":fanpage_facebook",$fanpage_facebook);
        $statement->bindParam(":instagram", $instagram);
        $statement->bindParam(":perfil_facebook", $perfil_facebook);
        $statement->bindParam(":twitter", $twitter);
        $statement->bindParam(":celular", $celular);
        $statement->bindParam(":correo", $correo);
        
        //echo $fanpage_facebook,$instagram,$perfil_facebook,$twitter,$celular,$correo;
        if(!$statement){
            return "Error al Modificar";
        }else{
            $statement->execute();
            return "Informacion de contacto actualizada Correctamente";
        }
    }
}
?>