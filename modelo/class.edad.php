<?php
class edad{
    
    public function ListarEdades(){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        $sql="SELECT * FROM edad";
        $statement=$conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    
    //InsertarEdad
    public function insertarEdad($newedad,$estado){
        $modelo = new Conexion();
        $conexion = $modelo->get_conexion();
        $sql="insert into edad(edad_nombre,estado_id) values (:edad,:estado)";
        $statement=$conexion->prepare($sql);
        $statement->bindParam(':edad',$newedad);
        $statement->bindParam(':estado', $estado);
        if(!$statement){
            return "Error al crear la edad";
        }else{
            $statement->execute();
            return "Edad Creada Correctamente";
        }
    }
    
}
?>