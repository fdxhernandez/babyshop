<?php
class producto{
    
    //InsertarUsuario
    public function InsertarProducto($prod_nombre,$prod_descripcion,$prod_urlimg,$precio,$id_edad,$id_genero,$id_estpro){
        $modelo = new Conexion();
        $conexion = $modelo->get_conexion();
        $sql="INSERT INTO producto( prod_nombre, prod_descripcion, prod_urlimg, precio, id_edad, id_genero, id_estpro) values (:prod_nombre, :prod_descripcion, :prod_urlimg, :precio, :id_edad, :id_genero, :id_estpro)";

        $statement=$conexion->prepare($sql);
        $statement->bindParam(':prod_nombre',$prod_nombre);
        $statement->bindParam(':prod_descripcion', $prod_descripcion);
        $statement->bindParam(':prod_urlimg', $prod_urlimg);
        $statement->bindParam(':precio', $precio);
        $statement->bindParam(':id_edad', $id_edad);
        $statement->bindParam(':id_genero', $id_genero);
        $statement->bindParam(':id_estpro', $id_estpro);
        if(!$statement){
            return "Error al crear el producto";
        }else{
            $statement->execute();
            return "Producto Creado Correctamente";
        }
    }
    
    //mostrar todos los usuarios con estado 1=activo
    public function listarProducto() {
        $rows=NULL;
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql = "SELECT producto.prod_id, producto.prod_nombre, producto.prod_descripcion, producto.prod_urlimg, producto.precio, edad.edad_nombre, genero.gen_nombre, estado_producto.estpro_nombre FROM producto INNER JOIN edad ON producto.id_edad=edad.edad_id INNER JOIN genero ON producto.id_genero=genero.gen_id INNER JOIN estado_producto on producto.id_estpro=estado_producto.estpro_id ORDER BY prod_id";
        $statement = $conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
//        while ($result=$statement->fetch()){
//            $rows[]=$result;
//        }
        return $rows;
    }

        public function listarProductobusq($busca) {
        $rows=NULL;
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql = "SELECT producto.prod_id, producto.prod_nombre, producto.prod_descripcion, producto.prod_urlimg, producto.precio, edad.edad_nombre, genero.gen_nombre, estado_producto.estpro_nombre FROM producto INNER JOIN edad ON producto.id_edad=edad.edad_id INNER JOIN genero ON producto.id_genero=genero.gen_id INNER JOIN estado_producto on producto.id_estpro=estado_producto.estpro_id WHERE producto.prod_nombre LIKE '%".$busca."%' OR producto.precio LIKE '%".$busca."%' OR edad.edad_nombre LIKE '%".$busca."%' OR genero.gen_nombre LIKE '%".$busca."%' OR estado_producto.estpro_nombre 
            LIKE '%".$busca."%' ";
            //echo $sql;
        $statement = $conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
//        while ($result=$statement->fetch()){
//            $rows[]=$result;
//        }

        return $rows;

    }
    

    //listar Producto a editar
    public function CargaProdEDICT($idP){
        $rows=NULL;
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        //$sql = "SELECT * FROM producto WHERE prod_id= :idP";
         $sql = "SELECT producto.prod_id, producto.prod_nombre, producto.prod_descripcion, producto.prod_urlimg, producto.precio,producto.id_edad, edad.edad_nombre,producto.id_genero, genero.gen_nombre,producto.id_estpro, estado_producto.estpro_nombre FROM producto INNER JOIN edad ON producto.id_edad=edad.edad_id INNER JOIN genero ON producto.id_genero=genero.gen_id INNER JOIN estado_producto on producto.id_estpro=estado_producto.estpro_id WHERE prod_id= :idP";
        $statement = $conexion->prepare($sql);
        $statement->bindParam(':idP', $idP);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
        
        }
    //actualizacion de Producto
    public function actualizarProducto($id,$prod_nombre,$prod_descripcion,$img,$precio,$id_edad,$id_genero,$id_estpro) {
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql="UPDATE producto SET prod_nombre=:prod_nombre, prod_descripcion=:prod_descripcion, prod_urlimg=:prod_urlimg, precio=:precio, id_edad=:id_edad, id_genero=:id_genero, id_estpro=:id_estpro  WHERE prod_id= :id";
        $statement= $conexion->prepare($sql);
        $statement->bindParam(":id",$id);
        $statement->bindParam(':prod_nombre',$prod_nombre);
        $statement->bindParam(':prod_descripcion', $prod_descripcion);
        $statement->bindParam(':prod_urlimg', $img);
        $statement->bindParam(':precio', $precio);
        $statement->bindParam(':id_edad', $id_edad);
        $statement->bindParam(':id_genero', $id_genero);
        $statement->bindParam(':id_estpro', $id_estpro);
        //echo $sql;
        if(!$statement){
            return "Error al modificar";
        }else{
            $statement->execute();
            return "Producto modificado con exito";
        }
        
    }
    //Eliminacion de Producto
    public function Eliminaproducto($iddel){
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql="DELETE FROM producto WHERE prod_id= :id";
        $statement= $conexion->prepare($sql);
        $statement->bindParam(":id",$iddel);
        if(!$statement){
            return "Error al Eliminar producto";
        }else{
            $statement->execute();
            return "1";
        }
    }
    
}
?>
