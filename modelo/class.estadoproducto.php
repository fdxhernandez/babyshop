<?php
class estadoProducto{
    
    public function Listarestadoproducto(){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        $sql="SELECT * FROM estado_producto";
        $statement=$conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    
    //InsertarEdad
    public function insertarEstadoProducto($nombre){
        $modelo = new Conexion();
        $conexion = $modelo->get_conexion();
        $sql="insert into estado_producto(estpro_nombre) values (:nombre)";
        $statement=$conexion->prepare($sql);
        $statement->bindParam(':nombre',$nombre);
        if(!$statement){
            return "Error al crear el Estado";
        }else{
            $statement->execute();
            return "Estado Creado Correctamente";
        }
    }
    
}
?>