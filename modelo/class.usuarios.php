<?php
class consultas{
    
    //InsertarUsuario
    public function insertarUsuario($nombre,$correo,$password,$id_rol,$estado){
        $modelo = new Conexion();
        $conexion = $modelo->get_conexion();
        $sql="insert into usuario(usu_nombre, usu_correo, usu_password, rol_id, estado_id) values (:nombre,:correo,:password,:id_rol,:estado)";
        $statement=$conexion->prepare($sql);
        $statement->bindParam(':nombre',$nombre);
        $statement->bindParam(':correo', $correo);
        $statement->bindParam(':password', $password);
        $statement->bindParam(':id_rol', $id_rol);
        $statement->bindParam(':estado', $estado);
        if(!$statement){
            return "Error al crear el usuario";
        }else{
            $statement->execute();
            return "Usuario Creado Correctamente";
        }
    }
    
    //mostrar todos los usuarios con estado 1=activo
    public function listarusuarios() {
        $rows=NULL;
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql = "SELECT id, nombre, correo, id_rol  FROM usuario  WHERE estado= 1 order by id ";
        $sql3 = "SELECT usuario.usu_id, usuario.usu_nombre, usuario.usu_correo, usuario.rol_id, roles.rol_nombre,estado.esta_nombre  FROM usuario INNER JOIN roles ON usuario.rol_id=roles.rol_id INNER JOIN estado ON usuario.estado_id=estado.esta_id";
        $statement = $conexion->prepare($sql3);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
//        while ($result=$statement->fetch()){
//            $rows[]=$result;
//        }
        return $rows;
    }
    
    
    //eliminar o cambiar estado de un usuario a inactivo
    public function DeshabilitarUsuario($id){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        //$sql="DELETE FROM usuario WHERE id= :id";
        $sql="UPDATE usuario SET estado_id = 2 WHERE usu_id =:id ";
        $statement=$conexion->prepare($sql);
        $statement->bindParam(':id', $id);
        if(!$statement){
            echo "<script languaje='javascript'>alert('Error al eliminar Usuario')</script>";
        }else{
            echo "<script languaje='javascript'>alert('Eliminado correctamente')</script>";
            $statement->execute();
        }
    }
    
     //habilitar o cambiar estado de un usuario a activo
    public function habilitarUsuario($id){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        //$sql="DELETE FROM usuario WHERE id= :id";
        $sql="UPDATE usuario SET estado_id = 1 WHERE usu_id =:id ";
        $statement=$conexion->prepare($sql);
        $statement->bindParam(':id', $id);
        if(!$statement){
            echo "<script languaje='javascript'>alert('Error al eliminar Usuario')</script>";
        }else{
            echo "<script languaje='javascript'>alert('Eliminado correctamente')</script>";
            $statement->execute();
        }
    }
    //listar usuario a editar
    public function CargarUsuario($id){
        $rows=NULL;
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql = "SELECT id, nombre, correo, id_rol  FROM usuario  WHERE estado= 1 order by id ";
        $sql3 = "SELECT * FROM usuario WHERE usu_id=:id";
        $statement = $conexion->prepare($sql3);
        $statement->bindParam(":id", $id);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
        }
    //actualizacion de usuario
    public function ModificarUsuario($id, $nombre, $correo, $password,$rol,$estado) {
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $sql="UPDATE usuario SET usu_nombre=:nombre,usu_correo=:correo,usu_password=:password, rol_id=:rol, estado_id=:estado where usu_id=:id";
        $statement= $conexion->prepare($sql);
        $statement->bindParam(":id",$id);
        $statement->bindParam(":nombre",$nombre);
        $statement->bindParam(":correo",$correo);
        $statement->bindParam(":password",$password);
        $statement->bindParam(":rol",$rol);
        $statement->bindParam(":estado",$estado);
        if(!$statement){
            return "Error al modificar";
        }else{
            $statement->execute();
            return "Usuario modificado con exito";
        }
        
    }

    function login($user,$pass){
        $modelo= new Conexion();
        $conexion= $modelo->get_conexion();
        $query="SELECT usuario.usu_id, usuario.usu_nombre, usuario.usu_correo, usuario.usu_password, roles.rol_nombre FROM usuario INNER JOIN roles ON usuario.rol_id=roles.rol_id WHERE usuario.usu_correo=:user && usuario.estado_id=1";
        $statement= $conexion->prepare($query);
        $statement->bindParam(":user",$user);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        //print_r($rows);
        if (count($rows)) {
            for($i=0;$i<count($rows);$i++){
                if($rows[$i]['usu_password']==$pass){
                    session_start();
                    $_SESSION["user"]["id"]=$rows[$i]['usu_id'];
                    $_SESSION["user"]["nombre"]=$rows[$i]['usu_nombre'];
                    $_SESSION["autenticado"]= "yes";
                    $_SESSION["rol"]=$rows[$i]['rol_nombre'];
                    return 1;
                }else{
                    return "Usuario o Clave erroneo";
                }
            }
        }else{
            return "Usuario o Clave erroneo";
        }
}
    
}
?>
