<?php
class genero{
    
    public function ListarGenero(){
        $modelo= new Conexion();
        $conexion=$modelo->get_conexion();
        $sql="SELECT * FROM genero";
        $statement=$conexion->prepare($sql);
        $statement->execute();
        $rows=$statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    
    //InsertarEdad
    public function insertarGenero($newgenero,$estado){
        $modelo = new Conexion();
        $conexion = $modelo->get_conexion();
        $sql="insert into genero(gen_nombre,estado_id) values (:genero,:estado)";
        $statement=$conexion->prepare($sql);
        $statement->bindParam(':genero',$newgenero);
        $statement->bindParam(':estado', $estado);
        if(!$statement){
            return "Error al crear el Genero";
        }else{
            $statement->execute();
            return "Genero Creado Correctamente";
        }
    }
    
}
?>