
-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 08, 2016 at 01:35 PM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a4688779_fer`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacto`
--

CREATE TABLE `contacto` (
  `fanpage_facebook` varchar(50) DEFAULT NULL,
  `instagram` varchar(50) DEFAULT NULL,
  `perfil_facebook` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `googleplus` varchar(50) DEFAULT NULL,
  `youtube` varchar(50) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `correo` varchar(150) DEFAULT NULL,
  `id_info` int(1) NOT NULL,
  PRIMARY KEY (`id_info`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacto`
--

INSERT INTO `contacto` VALUES('http://facebook.com/pages/', 'http://instagram.com/inst', 'http://facebook.com/perfil', '', NULL, NULL, '12345', '1@bsk.co', 1);

-- --------------------------------------------------------

--
-- Table structure for table `edad`
--

CREATE TABLE `edad` (
  `edad_id` int(2) NOT NULL AUTO_INCREMENT,
  `edad_nombre` varchar(50) NOT NULL,
  `estado_id` int(2) NOT NULL,
  PRIMARY KEY (`edad_id`),
  KEY `estado_id` (`estado_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `edad`
--

INSERT INTO `edad` VALUES(1, '1 aÃ±o', 1);

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `esta_id` int(1) NOT NULL AUTO_INCREMENT,
  `esta_nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`esta_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` VALUES(2, 'inactivo');
INSERT INTO `estado` VALUES(1, 'activo');

-- --------------------------------------------------------

--
-- Table structure for table `estado_producto`
--

CREATE TABLE `estado_producto` (
  `estpro_id` int(2) NOT NULL AUTO_INCREMENT,
  `estpro_nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`estpro_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `estado_producto`
--

INSERT INTO `estado_producto` VALUES(1, 'Disponible');
INSERT INTO `estado_producto` VALUES(2, 'No Disponible');

-- --------------------------------------------------------

--
-- Table structure for table `genero`
--

CREATE TABLE `genero` (
  `gen_id` int(2) NOT NULL AUTO_INCREMENT,
  `gen_nombre` varchar(20) NOT NULL,
  `estado_id` int(2) NOT NULL,
  PRIMARY KEY (`gen_id`),
  KEY `estado_id` (`estado_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `genero`
--

INSERT INTO `genero` VALUES(1, 'niÃ±o', 1);
INSERT INTO `genero` VALUES(2, 'niÃ±a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `prod_id` int(2) NOT NULL AUTO_INCREMENT,
  `prod_nombre` varchar(100) NOT NULL,
  `prod_descripcion` varchar(500) NOT NULL,
  `prod_urlimg` varchar(250) NOT NULL,
  `precio` int(10) NOT NULL,
  `id_edad` int(2) NOT NULL,
  `id_genero` int(2) NOT NULL,
  `id_estpro` int(2) NOT NULL,
  PRIMARY KEY (`prod_id`),
  KEY `id_estpro` (`id_estpro`),
  KEY `id_genero` (`id_genero`),
  KEY `id_edad` (`id_edad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` VALUES(1, 'casco', 'casco shaft samurai', '../img/portfolio/thumbnails/casco111.jpeg', 190, 1, 1, 1);
INSERT INTO `producto` VALUES(2, 'casco prueba', 'casco', '../img/portfolio/thumbnails/casco prueba11.jpeg', 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `rol_id` int(2) NOT NULL AUTO_INCREMENT,
  `rol_nombre` varchar(50) NOT NULL,
  `estado_id` int(2) NOT NULL,
  PRIMARY KEY (`rol_id`),
  KEY `estado_id` (`estado_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` VALUES(2, 'superadmin', 1);
INSERT INTO `roles` VALUES(1, 'administrador', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `usu_id` int(2) NOT NULL AUTO_INCREMENT,
  `usu_nombre` varchar(100) NOT NULL,
  `usu_correo` varchar(150) NOT NULL,
  `usu_password` varchar(75) NOT NULL,
  `rol_id` int(2) NOT NULL,
  `estado_id` int(2) NOT NULL,
  PRIMARY KEY (`usu_id`),
  KEY `rol_id` (`rol_id`),
  KEY `estado_id` (`estado_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` VALUES(5, 'Luis Fernando', 'dos@2', 'QMdMTT0DuSnqp/uBVRXWocseu7wokv64dflqU+uTm78=', 2, 1);
INSERT INTO `usuario` VALUES(6, 'Kelly agudelo', 'uno@1', 'hE0CDvvFlzIezwbnbCIUiJ+01hjA/MAs2uzQA50ZKLQ=', 1, 2);
